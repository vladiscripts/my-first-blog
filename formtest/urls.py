#!/usr/bin/env python
# coding: utf-8
from django.conf.urls import url

from . import views

urlpatterns = [
    # ex: /polls/
    url(r'^$', views.get_name, name='index'),
]
